# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Purpose License v2

require freedesktop-desktop gsettings gtk-icon-cache github [ user=lwindolf tag=v${PV} ]
require autotools [ supported_autoconf=[ 2.5 ]  supported_automake=[ 1.13 ] ]

SUMMARY="Linux Feed Reader"
HOMEPAGE="http://lzone.de/liferea/"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="gobject-introspection
    libindicate [[ description = [ Use libindicate for Ayatana notifications ] ]] libnotify
    ( linguas: ar ast be@latin bg ca cs da de el es eu fi fr gl he hu it ja ko lt lv mk nl pl pt
               pt_BR ro ru sk sq sv tr uk vi zh_CN zh_TW )
"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.40.4]
        virtual/pkg-config[>=0.20]
    build+run:
        x11-libs/gtk+:3[>=3.4.0][gobject-introspection?]
        dev-libs/glib:2[>=2.28]
        x11-libs/pango[>=1.4.0]
        dev-libs/libxml2[>=2.6.27]
        dev-libs/libxslt[>=1.1.19]
        dev-db/sqlite:3[>=3.7]
        gnome-desktop/libsoup:2.4[>=2.28.2]
        net-libs/webkit:3.0
        core/json-glib
        gnome-desktop/gsettings-desktop-schemas
        dev-libs/libpeas:1.0[>=1.0.0]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.3] )
        libindicate? ( dev-libs/libindicate[>=0.6] )
        libnotify? ( x11-libs/libnotify[>=0.7] )
"

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'gobject-introspection introspection'
    'libindicate'
    'libnotify'
)

src_prepare() {
    intltoolize --force --copy
    autotools_src_prepare
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

